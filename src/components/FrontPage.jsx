import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { BsPencilFill } from "react-icons/bs";
// BsPencilFill

class FrontPage extends Component {
  state = {};
  render() {
    return (
      <div className="landingPage">
        <header className="header-landing-page">
          <div className="div-inside-header">
            <BsPencilFill className="pencilLogo" />
            <NavLink className="Nav-link" to="/App">
              Start writing
            </NavLink>
          </div>
        </header>
        <section className="section-logo">
          <img
            className="stackeditLogo"
            data-attachment-id="61186"
            data-permalink="https://itsfoss.com/stackedit_markdown_editor/"
            data-orig-file="https://itsfoss.com/wp-content/uploads/2019/07/stackedit_markdown_editor.png"
            data-orig-size="800,480"
            data-comments-opened="1"
            data-image-meta='{"aperture":"0","credit":"","camera":"","caption":"","created_timestamp":"0","copyright":"","focal_length":"0","iso":"0","shutter_speed":"0","title":"","orientation":"0"}'
            data-image-title="stackedit_markdown_editor"
            data-image-description=""
            data-image-caption="<p>StackEdit Markdown Editor</p>
"
            data-medium-file="https://itsfoss.com/wp-content/uploads/2019/07/stackedit_markdown_editor-300x180.png"
            data-large-file="https://itsfoss.com/wp-content/uploads/2019/07/stackedit_markdown_editor-800x480.png"
            src="https://itsfoss.com/wp-content/uploads/2019/07/stackedit_markdown_editor.png"
            alt="StackEdit Markdown Editor"
            class="wp-image-61186 jetpack-lazy-image jetpack-lazy-image--handled"
            srcset="https://itsfoss.com/wp-content/uploads/2019/07/stackedit_markdown_editor.png 800w, https://itsfoss.com/wp-content/uploads/2019/07/stackedit_markdown_editor-300x180.png 300w, https://itsfoss.com/wp-content/uploads/2019/07/stackedit_markdown_editor-768x461.png 768w"
            data-lazy-loaded="1"
            sizes="(max-width: 800px) 100vw, 800px"
            loading="eager"
            width="800"
            height="480"
          ></img>
        </section>
      </div>
    );
  }
}

export default FrontPage;

// activeStyle={{ color: "gray" }} exact={true}
